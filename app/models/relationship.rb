class Relationship < ApplicationRecord
  belongs_to :follower, class_name: "User"
  belongs_to :followed, class_name: "User"

  validates :follower_id, presence: true
  validates :followed_id, presence: true

  def validate_relationship
    current_user = User.find(follower_id)
    if current_user.following?(followed_id)
      errors.add(:followed_id, "user id is already followed")
    end
  end
end

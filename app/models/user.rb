class User < ApplicationRecord
  after_create :send_email
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :tweets, dependent: :destroy
  has_many :friendships
  has_many :friends, :through => :friendships

  has_many :relationships, source: "follower_id"
  has_and_belongs_to_many :followers, class_name: "User", foreign_key: "followed_id", join_table: "relationships", association_foreign_key: "follower_id"
  has_and_belongs_to_many :following, class_name: "User", foreign_key: "follower_id", join_table: "relationships", association_foreign_key: "followed_id"

  validates :email, :first_name, :last_name, :username, presence: true
  validates :email, :username, uniqueness: true

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def full_name
    "#{first_name} #{last_name}"
  end

  def follow(other_user)
    following << other_user unless following?(other_user)
  end

  def unfollow(other_user)
    following.destroy(User.find(other_user.id))
  end

  def following?(other_user)
    following.include?(other_user)
  end

  def send_email
    UserMailer.welcome_email(self).deliver_later
  end
end

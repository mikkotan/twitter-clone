module ApplicationHelper
  def resource_name
    :user
  end

  def resource
    @user ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def resource_class
    User
  end

  def get_unfollowed_users
    ids = current_user.following.ids << current_user.id
    @unfollowed_users = User.where.not(id: ids)
  end

end

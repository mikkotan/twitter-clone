class TweetsController < ApplicationController
  before_action :set_tweet, only: [:update, :destroy]
  def index
  end

  def show
  end

  def new
  end

  def create
    @tweet = Tweet.new(tweet_params.merge(user_id: current_user.id))

    if @tweet.save
      redirect_to home_timeline_path, notice: 'Tweet was successfully created.'
    else
      redirect_to home_timeline_path, notice: 'Tweet was not created.'
    end
  end

  def edit
  end

  def update
  end

  def destroy
#  @tweet = Tweet.new(tweet_params.merge(user_id: current_user.id))
    @tweet.destroy
      redirect_to home_timeline_path, notice: 'tweet was successfully deleted'

  end

  private
  def set_tweet
    @tweet = Tweet.find(params[:id])
  end

  def tweet_params
    params.require(:tweet).permit(:content)
  end
end

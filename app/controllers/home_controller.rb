class HomeController < ApplicationController
  include ApplicationHelper
  before_action :get_unfollowed_users, only: [:timeline]

  def index
    if user_signed_in?
      redirect_to home_timeline_path
    end
  end

  def timeline
    ids = current_user.following.ids << current_user.id
    @tweet = Tweet.new
    @tweets = Tweet.joins(:user).where(user_id: ids).order(created_at: :desc)
    @users = @unfollowed_users.limit(5).shuffle
  end
end

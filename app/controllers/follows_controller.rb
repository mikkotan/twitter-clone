class FollowsController < ApplicationController
  include ApplicationHelper
  skip_before_action :verify_authenticity_token
  before_action :authenticate_user!
  before_action :get_unfollowed_users, only: [:index]
  def index

  end

  def create
    @other_user = User.find(params[:id])
    if current_user.follow(@other_user)
      flash[:notice] = "Followed '@#{@other_user.username}' successfully"
    else
      flash[:error] = "Follow failed"
    end
    redirect_to root_url
  end

  def destroy
    @other_user = User.find(params[:id])
    if current_user.unfollow(@other_user)
      flash[:notice] = "Unfollowed '@#{@other_user.username}' successfully"
    else
      flash[:error] = "Unfollowed failed"
    end
    redirect_to root_url
  end
end

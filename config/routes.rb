Rails.application.routes.draw do
  resources :tweets
  resources :friendships
  resources :follows, only: ['index', 'create', 'destroy']
  resources :profiles, only: ['show']
  get 'home/timeline'
  devise_for :users, :controllers => {
    registrations: 'users/registrations',
  }

  root to: 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

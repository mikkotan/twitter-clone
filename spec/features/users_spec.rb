require 'rails_helper'

include Warden::Test::Helpers
Warden.test_mode!


describe 'User Registration', type: :feature do
  describe 'Creating Users' do
    index_sign_up
    context 'When the form is valid' do
      before do
        fill_in 'First name', with: 'twitter'
        fill_in 'Last name', with: 'user'
        fill_in 'Username', with: 'username'
        fill_in 'Email', with: 'twitteruser@example.com'
        fill_in 'Password', with: 'password'
        fill_in 'Password confirmation', with: 'password'
        click_button 'Sign up'
      end

      it 'creates the user' do
        expect(page).to have_content 'Welcome! You have signed up successfully.'
      end
    end

    context 'When the form is invalid' do
      context 'and the form is blank' do
        before do
          click_button 'Sign up'
        end

        it 'returns an error page' do
          expect(page).to have_content "First name can't be blank"
          expect(page).to have_content "Last name can't be blank"
          expect(page).to have_content "Username can't be blank"
          expect(page).to have_content "Email can't be blank"
          expect(page).to have_content "Password can't be blank"
        end
      end

      context 'Password does not match password_confirmation' do
        before do
          fill_in 'First name', with: 'twitter'
          fill_in 'Last name', with: 'user'
          fill_in 'Username', with: 'username'
          fill_in 'Email', with: 'twitteruser@example.com'
          fill_in 'Password', with: 'password'
          fill_in 'Password confirmation', with: 'password3'
          click_button 'Sign up'
        end
        it 'returns an error page' do
          expect(page).to have_content "Password confirmation doesn't match Password"
        end
      end

      context 'Username already exists' do
        let!(:user) { FactoryGirl.create(:user, username: 'username') }

        before do
          fill_in 'First name', with: 'twitter'
          fill_in 'Last name', with: 'user'
          fill_in 'Username', with: 'username'
          fill_in 'Email', with: 'twitteruser@example.com'
          fill_in 'Password', with: 'password'
          fill_in 'Password confirmation', with: 'password'
          click_button 'Sign up'
        end
        it 'returns an error page' do
          expect(page).to have_content 'Username has already been taken'
        end
      end

      context 'Email already exists' do
        let!(:user) { FactoryGirl.create(:user, email: 'twitteruser@example.com') }

        before do
          fill_in 'First name', with: 'twitter'
          fill_in 'Last name', with: 'user'
          fill_in 'Username', with: 'username'
          fill_in 'Email', with: 'twitteruser@example.com'
          fill_in 'Password', with: 'password'
          fill_in 'Password confirmation', with: 'password'
          click_button 'Sign up'
          end
        it 'returns an error page' do
          expect(page).to have_content 'Email has already been taken'
        end
      end
    end
  end
end

describe 'User Login', type: :feature do
  describe 'User login page' do
    context 'when the form is valid' do
      index_sign_in
      log_in
      it 'logins the user' do
        expect(page).to have_content 'Profile'
      end
    end

    context 'when the form is invalid' do
      context 'when the email/password is blank' do
        index_sign_in
          it 'returns an error page' do
            click_button 'Log in'
            expect(page).to have_content "Invalid Email or password."
          end
      end
    end

    context 'when the user attempts to logout' do
      index_sign_in
      log_in
      it 'user will redirect to home page' do
        click_link('Log out', match: :first)
        expect(page).to have_content 'Hello World'
      end
    end
  end
end

describe 'User tweets', type: :feature do
  describe 'Users Timeline' do
    index_sign_in
    log_in
    context 'When the user post a timeline' do
      before do
        fill_in 'tweet_content', with: 'Test Tweet'
        click_button 'Tweet'
      end

      it 'User input tweet' do
        expect(page).to have_content ' Tweet was successfully created.'
      end

      it 'User delete tweet', js: true do
        click_link 'Delete'
        page.driver.accept_js_confirms!
        expect(page).to have_content 'tweet was successfully deleted'
      end
    end
  end
end

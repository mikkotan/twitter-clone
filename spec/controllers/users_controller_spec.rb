require 'rails_helper'

describe Users::SessionsController do
  login_user
  it 'User Log in Functionality' do
    expect(subject.current_user).to_not eq(nil)
  end
end

describe HomeController do
  login_user
  it 'should get index' do
    get 'index'
    expect(response).to redirect_to home_timeline_path
  end
end

describe Users::RegistrationsController do
  context 'When the User Edit Profile' do
    login_user
    it 'User can edit profile' do
      params = {:user => {first_name: "John", last_name: "Snow", username: "johnsnow", email: "johnsnow@example.com",
        password: "johnsnow123", password_confirmation: "johnsnow123", current_password: "password" } }
      patch :update, params: params
      expect(response).to redirect_to root_path
      expect(flash[:notice]).to eq "Your account has been updated successfully."
    end
  end
    context 'when the Confirm Password is blank' do
      login_user
      it 'returns invalid form' do
        params = {:user => {first_name: "John", last_name: "Snow", username: "johnsnow", email: "johnsnow@example.com",
          password: "johnsnow123", password_confirmation: "johnsnow12", current_password: "" } }
        patch :update, params: params
         expect(response).to redirect_to user_registration_path
        expect(flash[:error]).to eq "Current password can't be blank"
      end
    end
end



describe TweetsController do
  login_user
  it 'User post a tweet in timeline' do
    params = {:tweet => {content: "Hello world of tweets"} }
    get 'create', params: params
    expect(response).to redirect_to home_timeline_path
    expect(flash[:notice]).to eq "Tweet was successfully created."
  end
end

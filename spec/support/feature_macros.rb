module FeatureMacros
  def index_sign_up
    before(:each) do
      # @request.env["devise.mapping"] = Devise.mappings[:user]
      visit '/'
      click_button 'Sign up'
    end
  end
  def index_sign_in
    before(:each) do
      # @request.env["devise.mapping"] = Devise.mappings[:user]
      visit '/'
      click_button 'Sign in'
    end
  end
  def log_in
    before(:each) do
    user = FactoryGirl.create(:user)
    login_as(user, :scope => :user)
    click_button 'Log in'
    end
  end
end

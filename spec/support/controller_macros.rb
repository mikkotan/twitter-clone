module ControllerMacros
  def login_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      user = FactoryGirl.create(:user)
      sign_in user
    end
  end
  def sign_up
    before(:each) do
      # @request.env["devise.mapping"] = Devise.mappings[:user]
      visit '/'
      click_button 'Sign up'
    end
  end
  def sign_in
    before(:each) do
      # @request.env["devise.mapping"] = Devise.mappings[:user]
      visit '/'
      click_button 'Sign in'
    end
  end
end

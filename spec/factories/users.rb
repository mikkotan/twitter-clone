FactoryGirl.define do
  factory :user do
    first_name "John"
    last_name "Doe"
    sequence(:username) {|n| "username#{n + 1}"}
    sequence(:email) {|n| "twitteruser#{n + 1}@example.com"}
    password "password"
  end
end

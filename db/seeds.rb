# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create([
    {
      first_name: 'test',
      last_name: 'user1',
      username: 'testuser1',
      email: 'testuser1@sample.com',
      password: 'password'
    },
    {
      first_name: 'test',
      last_name: 'user2',
      username: 'testuser2',
      email: 'testuser2@sample.com',
      password: 'password'
    }
  ])

20.times do
  first_name = Faker::Name.first_name
  last_name = Faker::Name.last_name
  User.create({
    first_name: first_name,
    last_name: last_name,
    email: Faker::Internet.email("#{first_name}.#{last_name}".downcase),
    username: "#{first_name}#{last_name}",
    password: 'password'
    })
end
